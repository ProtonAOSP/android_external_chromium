# Ungoogled Chromium AOSP Integration

This repository contains the latest prebuilds of Ungoogled Chromium from https://github.com/ungoogled-software/ungoogled-chromium-android/releases integrated with the AOSP build system.

## Notes

The set of Trichrome packages (TrichromeLibrary, TrichromeChrome, and TrichromeWebView) is preferred but, for now, Ungoogled Chromium doesn't build TrichromeWebView for arm or x86 so we use SystemWebView on those platforms.
